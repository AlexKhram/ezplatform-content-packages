# eZ Platform Content Packages Screenshots

## Export

![Export subtree form](./img/export_subtree_form.png)

![Export Content Types form](./img/export_content_types_form.png)

![Export progress](./img/export_subtree.png)

## Import

![Import subtree form](./img/import_subtree_form.png)

![Import confirm](./img/import_confirm.png)

![Import progress](./img/import_subtree.png)
