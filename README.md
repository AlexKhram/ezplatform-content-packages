# eZ Platform Content Packages

[![Downloads](https://img.shields.io/packagist/dt/contextualcode/ezplatform-content-packages.svg?style=flat-square)](https://packagist.org/packages/contextualcode/ezplatform-content-packages)
[![Latest stable](https://img.shields.io/packagist/v/contextualcode/ezplatform-content-packages.svg?style=flat-square)](https://packagist.org/packages/contextualcode/ezplatform-content-packages)
[![License](https://img.shields.io/badge/license-GPL--2.0-orange?style=flat-square)](https://packagist.org/packages/contextualcode/ezplatform-content-packages)

This bundle provides a way to export and import content between different eZ Platform installations.

Here are [a few screenshots](doc/SCREENSHOTS.md) of the export/import forms and progress.

## Installation

Please see the [installation instructions](doc/INSTALL.md).

## Usage

After installing, there will be a new menu option under the "Content" tab that links to `/content_packages`.

### Exporting Content Packages

From the installation you wish to export from, visit the Content Packages export page (`/content_packages/export`).

Content Package types supported currently are Location subtrees, Content Types, and Tag trees.

Please note that the exports are user-specific. Normal eZ permissions apply, and no one else will be able to view your exports.

#### Subtree Export 

Binaries (files/images) are included. References to other location/content ids (in relation attributes, for example) are resolved to their new ids.

**Important**

Please note that if there is a reference to a content/location id outside of the subtree you are exporting, it will be left intact (instead of being exported or resolved to a new id). This is a limitation that we wish to fix in future versions.

##### Subtree Export Options:

There are some options on the form. Leaving them as their defaults is fine, but you may want to change them if you need to customize how the Content Package is generated.

- "Preserve Remote Ids":

    Controls whether or not remote ids will be included in the export.
    If not, then new remote ids will be assigned when importing.

- "Preserve Owner":

    Controls whether or not owners of objects will be included in the export.
    If not, then the user who imports this Content Package will be the owner.

- "Preserve Publication Date":

    Controls whether or not original publication dates will be included in the export.

- "Preserve Modification Date":

    Controls whether or not original modification dates will be included in the export. 

- "Preserve Section":

    Controls whether or not sections will be included in the export.

- "Preserve Object States":

    Controls whether or not object states will be included in the export.

- "Include File Binaries":

    If this is checked, the images/files in the export will be included in the Content Package.
    If this is unchecked, the images/files in the export will not be included, but the references to those images will still be in the migration files. This is useful if you want to put the files in your Content Package with `rsync` after the fact, for example.

- "Include File References":

    If this is checked, the images/files values in the export Migration Files will be left intact.
    If this is unchecked, the images/files values in the export will be removed. This is useful if you don't want to import files at all into the new system.

- "Exclude Fields from Export":

    These are comma-separated content class attributes to not include in the export. They can be of the form "attribute_identifier" or "class_identifier/attribute_identifier".

- "Attributes to Replace Hardcoded Location Ids In":

    This parameter is useful, for example, if you have a Rich Text attribute that contains hardcoded location ids. Pass the name of that attribute here, and the export will replace those hardcoded location ids with the correct, new ids (if those locations are also in the subtree being exported).

- "Attributes to Replace Hardcoded Content Ids In":

    Same as the "Attributes to Replace Hardcoded Location Ids In" parameter, but with content ids.
    
- "Add Brackets Around References in Fields":

    Because of how the migration files work, you most likely want this field to contain the attributes you added to "Attributes to Replace Hardcoded Location Ids In" and "Attributes to Replace Hardcoded Content Ids In".


### Importing Content Packages

After exporting a Content Package, you can take that `.zip` file and use it to import to a different eZ installation. Visit the Content Packages import page (`/content_packages/import`).

When importing a subtree Content Package, you will have to select the parent location id to import the subtree under.

## How It Works

This tool is built off of [Kaliop's eZ Migration Bundle](https://github.com/kaliop-uk/ezmigrationbundle), which is inspired by the [DoctrineMigrationsBundle](https://github.com/doctrine/DoctrineMigrationsBundle). 

Generating an export of a subtree has a few main steps:
1. Generate eZ Migration files for each location in our subtree
    - We generate "create" eZ Migration files and then "update" eZ Migration files. This lets us have references to ids in the subtree being exported/imported (in later steps), without circular dependency issues
2. Create references for each location/content id in the Migration files we just created
3. Replace all location/content ids with the references we just created
4. Replace the subtree's root node's parent location id with a placeholder, so this Content Package can be imported under any parent location 
5. Copy all of the binaries (images/files) referenced in our Migration files to our directory
6. Zip it all up, serve to user as a Content Package

Imports are a bit simpler:
1. Unzip the Content Package
2. Replace the placeholder for the parent location id (made in step 4 of the export) with the given location id
3. Run the eZ Migrations

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
