<?php

namespace ContextualCode\EzPlatformContentPackagesBundle\Service;

use ContextualCode\EzPlatformContentPackagesBundle\Command\ImportCommand;
use ContextualCode\EzPlatformContentPackagesBundle\Entity\ContentPackageStatus;
use ContextualCode\EzPlatformContentPackagesBundle\Service\Override\ContentService;
use Doctrine\ORM\EntityManager;
use Exception;
use eZ\Publish\API\Repository\LocationService;
use eZ\Publish\API\Repository\SearchService;
use eZ\Publish\Core\Repository\ContentTypeService;
use Kaliop\eZMigrationBundle\Command\MigrateCommand;
use Netgen\TagsBundle\Core\Repository\TagsService;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Yaml\Yaml;
use ZipArchive;

class ContentPackagesImportService extends ContentPackagesService
{
    /** @var string|null */
    protected $currentImportDirectoryName = null;

    public function __construct(
        $config,
        SearchService $searchService,
        LocationService $locationService,
        ContentService $contentService,
        ContentTypeService $contentTypeService,
        EntityManager $entityManager,
        KernelInterface $kernel
    ) {
        parent::__construct(
            $config,
            $searchService,
            $locationService,
            $contentService,
            $contentTypeService,
            $entityManager,
            $kernel
        );
    }

    public function importAsync(
        ContentPackageStatus $entity,
        string $adminLogin,
        $parentId,
        string $packageId,
        string $type,
        string $logFilepath = null
    ) {
        if ($logFilepath === null) {
            $logFilepath = $this->getLogFilepath(
                $adminLogin,
                $packageId,
                true,
                true
            );
        }

        $fullLogFilepath = self::$PROJECT_ROOT_DIR . '/' . $logFilepath;
        if (!touch($fullLogFilepath)) {
            $entity->setInProgress(false);
            $entity->setError(
                str_replace('$FILE', $fullLogFilepath, self::ERROR_MESSAGES['log_write'])
            );
            $this->persistStatus($entity);

            return;
        }

        $command = 'cd ' . self::$PROJECT_ROOT_DIR . ' && nohup sh -c "';
        $command .= self::$PHP_COMMAND . ' -dmemory_limit=-1 ';
        $command .= self::$CONSOLE_PATH . ' ' . ImportCommand::COMMAND_NAME;
        $command .= ' --' . ImportCommand::PARAM_PARENT_ID . '=' . escapeshellarg($parentId);
        $command .= ' --' . ImportCommand::PARAM_ADMIN_LOGIN . '=' . escapeshellarg($adminLogin);
        $command .= ' --' . ImportCommand::PARAM_PACKAGE_ID . '=' . escapeshellarg($packageId);
        $command .= ' --' . ImportCommand::PARAM_TYPE . '=' . escapeshellarg($type);
        $command .= '" > ' . $logFilepath . ' 2>&1 &';
        exec($command, $output, $returnCode);

        if ($returnCode !== 0) {
            $this->setCommandFailed(ImportCommand::COMMAND_NAME, $returnCode, $entity);

            return;
        }
    }

    public function import(
        string $adminLogin,
        string $packageId,
        $parentId,
        string $type,
        $output = null
    ) {
        try {
            $this->output = $output;
            $this->setPackageId($packageId);
            $this->setParentId($parentId);
            $this->setAdminLogin($adminLogin);
            $this->setType($type);

            if ($parentId) {
                $this->writeln('<info>Importing under id ' . $this->getParentId() . '</info>');
            }

            $entity = $this->clearAndGetStatus();
            $entity->setInProgress(true);
            $this->persistStatus($entity);

            // don't clear the directories because the content package is in there
            $this->setupImportDirectories(
                null,
                null,
                false,
                false
            );

            $this->extractContentPackage(
                null,
                null,
                false
            );
            $migrateFilepath = $this->findMigrateFilepath();
            $this->updateStatus($entity, 10);

            if ($parentId) {
                // replace root id placeholder with the given id
                $this->replaceRootIdPlaceholder($migrateFilepath, $parentId);
                $this->updateStatus($entity, 20);
            }

            $this->createBinarySymlinks();
            $this->updateStatus($entity, 30);

            // run the actual migration
            $this->runMigration($migrateFilepath, $entity);
            $this->updateStatus($entity, 90);

            // remove files
            $userPackageDir = $this->getUserPackageDirectory(
                null,
                null,
                false,
                true
            );
            $this->clearDirectory($userPackageDir, [], [self::LOG_FILENAME]);

            $this->updateStatus($entity, 100);
            $entity->setInProgress(false);
            $this->persistStatus($entity);
            $this->writeln('');
            $this->writeln('<info>Done.</info>');
        } catch (Exception $e) {
            if (!isset($entity) || !($entity instanceof ContentPackageStatus)) {
                throw new Exception(self::ERROR_MESSAGES['entity'] . '(' . $e->getMessage() . ')');
            }

            $entity->setInProgress(false);
            $entity->setError($e->getMessage());
            $this->persistStatus($entity);
        }
    }

    protected function createBinarySymlinks()
    {
        $imagesDir = $this->getImagesDirectory(
            null,
            null,
            false,
            true
        );
        $mediaDir = $this->getMediaDirectory(
            null,
            null,
            false,
            true
        );

        symlink('files', $imagesDir);
        symlink('files', $mediaDir);
    }

    public function extractContentPackage(
        $adminLogin = null,
        $packageId = null,
        $noRootDir = false
    ) {
        $fullFilepath = $this->getZipFilepath(
            $adminLogin,
            $packageId,
            $noRootDir,
            true
        );
        if (!file_exists($fullFilepath)) {
            throw new Exception(
                str_replace(
                    '$FILEPATH',
                    $fullFilepath,
                    self::ERROR_MESSAGES['file_open']
                )
            );
        }
        $userPackageDir = $this->getUserPackageDirectory(
            $adminLogin,
            $packageId,
            $noRootDir,
            true
        );
        $success = false;
        $zip = new ZipArchive;
        if ($zip->open($fullFilepath)) {
            try {
                $success = $zip->extractTo($userPackageDir);
                $zip->close();
            } catch (Exception $e) {
                $success = false;
            }
        }
        if (!$success) {
            throw new Exception(self::ERROR_MESSAGES['content_package']);
        }
    }

    protected function findMigrateFilepath()
    {
        $types = [
            ContentPackagesService::SUBTREE_TYPE,
            ContentPackagesService::CONTENT_TYPES_TYPE,
            ContentPackagesService::TAGS_TYPE,
        ];
        foreach ($types as $type) {
            $migrateFilepathSearch = $this->getUserPackageDirectory(
                null,
                null,
                false,
                true
            ) . '/' . $type . '_*.yml';
            $migrateFilepaths = glob($migrateFilepathSearch);
            if ($migrateFilepaths !== false && count($migrateFilepaths) !== 0) {
                break;
            }
        }
        if ($migrateFilepaths === false || count($migrateFilepaths) === 0) {
            throw new Exception(self::ERROR_MESSAGES['content_package']);
        }

        return $migrateFilepaths[0];
    }

    protected function replaceRootIdPlaceholder(string $migrateFilepath, int $parentId)
    {
        $yaml = Yaml::parseFile($migrateFilepath);
        $type = $this->getType();
        $ref = null;
        switch ($type) {
            case ContentPackagesService::SUBTREE_TYPE:
                $ref = [
                    'type' => 'reference',
                    'mode' => 'set',
                    'identifier' => self::getLocationReferenceKey(self::ROOT_PLACEHOLDER_ID),
                    'value' => $parentId,
                ];
                break;
            case ContentPackagesService::TAGS_TYPE:
                $ref = [
                    'type' => 'reference',
                    'mode' => 'set',
                    'identifier' => self::getTagRemoteIdReferenceKey(self::ROOT_PLACEHOLDER_ID),
                    'value' => $parentId,
                ];
                break;
        }
        if ($ref) {
            array_unshift($yaml, $ref);
        }
        $yaml = Yaml::dump($yaml, 4, 4);
        file_put_contents($migrateFilepath, $yaml);
    }

    protected function runMigration(
        string $migrateFilepath,
        ContentPackageStatus $entity
    ) {
        // set special variable so we can skip validation errors
        // when we use our ContentService override
        $this->contentService->setIgnoreFieldValidationErrors(true);

        $this->writeln('Running command ' . MigrateCommand::COMMAND_NAME);

        $application = new Application($this->kernel);
        $application->setAutoExit(false);

        $params = [
            'command' => MigrateCommand::COMMAND_NAME,
            '--path' => [$migrateFilepath],
            '--admin-login' => $this->getAdminLogin(),
            '--no-interaction' => true,
            '--force' => true,
        ];
        $this->writeln('Params: ' . print_r($params, true));
        $input = new ArrayInput($params);
        $output = new BufferedOutput();
        try {
            $returnCode = $application->run($input, $output);
        } catch (Exception $e) {
            $this->setCommandFailed(MigrateCommand::COMMAND_NAME, '1', $entity);

            return;
        }

        $content = $output->fetch();
        $this->writeln(print_r($content, true));

        $this->contentService->setIgnoreFieldValidationErrors(false);

        if ($returnCode !== 0) {
            $this->setCommandFailed(MigrateCommand::COMMAND_NAME, $returnCode, $entity);
        }
    }

    // TODO: do we need $parentId here?
    public function moveImportFile(
        UploadedFile $file,
        string $adminLogin,
        $parentId,
        string $packageId
    ) {
        $this->setPackageId($packageId);
        $this->setParentId($parentId);
        $this->setAdminLogin($adminLogin);

        // clear the directories because this is a new import
        $this->setupImportDirectories(
            null,
            null,
            false,
            true
        );

        $file->move(
            $this->getUserPackageDirectory(
                null,
                null,
                false,
                true
            ),
            $this->getZipFilename($packageId)
        );
    }

    public function setupImportDirectories(
        $adminLogin = null,
        $packageId = null,
        $noRootDir = false,
        $clear = true
    ) {
        $this->currentImportDirectoryName = $this->getUserPackageDirectory(
            $adminLogin,
            $packageId,
            $noRootDir,
            true
        );
        $this->ensureDirectoryExists($this->currentImportDirectoryName);
        if ($clear) {
            $this->clearDirectory($this->currentImportDirectoryName, [], [self::LOG_FILENAME]);
        }

        $filesDir = $this->getFilesDirectory(
            $adminLogin,
            $packageId,
            $noRootDir,
            true
        );
        $this->ensureDirectoryExists($filesDir);
        if ($clear) {
            $this->clearDirectory($filesDir);
        }
    }
}
