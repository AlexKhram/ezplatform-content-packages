<?php

namespace ContextualCode\EzPlatformContentPackagesBundle\Service;

use ContextualCode\EzPlatformContentPackagesBundle\Command\ExportContentTypesCommand;
use ContextualCode\EzPlatformContentPackagesBundle\Command\ExportSubtreeCommand;
use ContextualCode\EzPlatformContentPackagesBundle\Command\ExportTagsCommand;
use ContextualCode\EzPlatformContentPackagesBundle\Entity\ContentPackageStatus;
use ContextualCode\EzPlatformContentPackagesBundle\FilterIterator\NoDotRecursiveFilterIterator;
use ContextualCode\EzPlatformContentPackagesBundle\FilterIterator\MigrateFilesRecursiveFilterIterator;
use ContextualCode\EzPlatformContentPackagesBundle\Service\Override\ContentService;
use Doctrine\ORM\EntityManager;
use Exception;
use eZ\Publish\API\Repository\LocationService;
use eZ\Publish\API\Repository\SearchService;
use eZ\Publish\API\Repository\Values\Content\Location;
use eZ\Publish\API\Repository\Values\Content\LocationQuery;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\API\Repository\Values\Content\Query\SortClause;
use eZ\Publish\Core\Repository\ContentTypeService;
use Netgen\TagsBundle\Core\Repository\TagsService;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Yaml\Yaml;
use ZipArchive;

class ContentPackagesExportService extends ContentPackagesService
{
    protected const REFERENCE_CONFIG = [
        [
            'type' => 'Location/Content',
            'file_regex' => '/(.*)content\.yml/',
            'references' => [
                [
                    'callback' => 'self::getLocationReferenceKey',
                    'callback_param' => 'location_id',
                    'attribute' => 'location_id',
                ],
                [
                    'callback' => 'self::getContentReferenceKey',
                    'callback_param' => 'location_id',
                    'attribute' => 'content_id',
                ],
                [
                    'callback' => 'self::getContentRemoteIdReferenceKey',
                    'callback_param' => 'location_id',
                    'attribute' => 'content_remote_id',
                ],
            ],
        ],
    ];

    protected const BINARY_KEYS = ['path:', 'image:'];

    protected const EMPTY_VALUES = [
        'xml' => "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<section xmlns=\"http://docbook.org/ns/docbook\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:ezxhtml=\"http://ez.no/xmlns/ezpublish/docbook/xhtml\" xmlns:ezcustom=\"http://ez.no/xmlns/ezpublish/docbook/custom\" version=\"5.0-variant ezpublish-1.0\"><para></para></section>\n",
        'destinationContentIds' => [],
        'destinationContentId' => null,
        'blocks' => [],
    ];

    /** @var string|null */
    protected $currentExportDirectoryName = null;
    /** @var RecursiveDirectoryIterator|null */
    protected $currentExportDirectoryMigrateFilesIterator = null;
    /** @var RecursiveDirectoryIterator|null */
    protected $currentExportDirectoryIterator = null;
    /** @var Location|null */
    protected $parentLocation = null;
    /** @var array */
    protected $contentMetadata = [];
    /** @var array */
    protected $contentTypeIds = [];
    /** @var array */
    protected $baseArguments = [];
    /** @var array */
    protected $references = [];
    /** @var string|null */
    protected $type = null;

    public function __construct(
        $config,
        SearchService $searchService,
        LocationService $locationService,
        ContentService $contentService,
        ContentTypeService $contentTypeService,
        EntityManager $entityManager,
        KernelInterface $kernel
    ) {
        parent::__construct(
            $config,
            $searchService,
            $locationService,
            $contentService,
            $contentTypeService,
            $entityManager,
            $kernel
        );
    }

    public function exportTagsAsync(
        ContentPackageStatus $entity,
        string $adminLogin,
        string $packageId,
        string $parentTagId,
        string $logFilepath = null
    ) {
        if ($logFilepath === null) {
            $logFilepath = $this->getLogFilepath(
                $adminLogin,
                $packageId,
                true,
                false
            );
        }
        try {
            $this->checkFullLogFilepath($logFilepath);
        } catch (Exception $e) {
            return;
        }

        $command = 'cd ' . self::$PROJECT_ROOT_DIR . ' && nohup sh -c "';
        $command .= self::$PHP_COMMAND . ' -dmemory_limit=-1 ';
        $command .= self::$CONSOLE_PATH . ' ' . ExportTagsCommand::$COMMAND_NAME;
        $command .= ' --' . ExportTagsCommand::PARAM_PARENT_TAG_ID . '=' . escapeshellarg($parentTagId);
        $command .= ' --' . ExportTagsCommand::PARAM_ADMIN_LOGIN . '=' . escapeshellarg($adminLogin);
        $command .= ' --' . ExportTagsCommand::PARAM_PACKAGE_ID . '=' . escapeshellarg($packageId);
        $command .= '" > ' . $logFilepath . ' 2>&1 &';
        exec($command, $output, $returnCode);

        if ($returnCode !== 0) {
            $this->setCommandFailed(ExportTagsCommand::$COMMAND_NAME, $returnCode, $entity);
            return;
        }
    }

    public function exportTags(
        string $adminLogin,
        string $packageId,
        string $parentTagId,
        $output = null
    ) {
        try {
            $this->output = $output;
            $this->setPackageId($packageId);
            $this->setAdminLogin($adminLogin);
            $this->setType(self::TAGS_TYPE);

            if (!($this->tagsService instanceof TagsService)) {
                throw new Exception('netgen/TagsBundle is not installed/enabled.');
            }

            $this->writeln('<info>Exporting tag subtree ' . $parentTagId . '</info>');

            $entity = $this->clearAndGetStatus();
            $entity->setInProgress(true);
            $this->persistStatus($entity);

            $this->updateStatus($entity, 5);
            $this->writeln('<info>Setting up directory...</info>');
            $this->setupExportDirectories();

            $this->setupMigrateCommandArguments();

            $this->updateStatus($entity, 10);
            $this->writeln('<info>Generating migrations for Tags...</info>');
            $this->generateMigrateFilesForTags($parentTagId);

            $this->updateStatus($entity, 50);
            $this->writeln('<info>Generating reference keys for ids...</info>');
            $parentTag = $this->tagsService->loadTag($parentTagId);
            if ($parentTag->hasParent()) {
                $parentTagParent = $this->tagsService->loadTag($parentTag->parentTagId);
                $parentTagParentRemoteId = $parentTagParent->remoteId;
            }
            else {
                $parentTagParentRemoteId = '0';
            }
            // this will be a placeholder so the import can be used under any tag tree
            $extraReferences = [
                [
                    'type' => 'tag_remote_id',
                    'old_hardcoded_value' => $parentTagParentRemoteId,
                    'new_reference_key' => self::getTagRemoteIdReferenceKey(self::ROOT_PLACEHOLDER_ID),
                ],
            ];
            $this->generateReferenceKeys($extraReferences);

            $this->updateStatus($entity, 60);
            $this->writeln('<info>Replacing ids with references...</info>');
            $this->replaceReferences();

            $this->updateStatus($entity, 70);
            $this->writeln('<info>Combining migrate files...</info>');
            $this->combineFiles();
            $this->updateStatus($entity, 80);

            $this->updateStatus($entity, 85);
            $this->writeln('<info>Creating metadata file...</info>');
            $delim = ',';
            $extraData = [
                ExportTagsCommand::PARAM_PARENT_TAG_ID => $parentTagId,
            ];
            $this->createMetadataFile(
                $this->generatePackageName([
                    'keyword' => $parentTag->keyword,
                    'id' => $parentTag->id,
                ]),
                $adminLogin,
                $packageId,
                false,
                $extraData
            );

            $this->updateStatus($entity, 90);
            $this->writeln('<info>Creating zip...</info>');
            $this->createZip();

            $this->updateStatus($entity, 100);
            $entity->setInProgress(false);
            $this->persistStatus($entity);
            $this->writeln('');
            $this->writeln('<info>Done.</info>');
        } catch (Exception $e) {
            if (!isset($entity) || !($entity instanceof ContentPackageStatus)) {
                throw new Exception(self::ERROR_MESSAGES['entity'] . '(' . $e->getMessage() . ')');
            }

            $entity->setInProgress(false);
            $entity->setError($e->getMessage());
            $this->persistStatus($entity);
        }
    }

    public function exportContentTypesAsync(
        ContentPackageStatus $entity,
        string $adminLogin,
        string $packageId,
        array $contentTypeIdentifiers,
        array $excludeAttributes = [],
        string $logFilepath = null
    ) {
        if ($logFilepath === null) {
            $logFilepath = $this->getLogFilepath(
                $adminLogin,
                $packageId,
                true,
                false
            );
        }
        try {
            $this->checkFullLogFilepath($logFilepath);
        } catch (Exception $e) {
            return;
        }

        $command = 'cd ' . self::$PROJECT_ROOT_DIR . ' && nohup sh -c "';
        $command .= self::$PHP_COMMAND . ' -dmemory_limit=-1 ';
        $command .= self::$CONSOLE_PATH . ' ' . ExportContentTypesCommand::$COMMAND_NAME;
        $command .= ' --' . ExportContentTypesCommand::PARAM_CONTENT_TYPE_IDENTIFIERS . '=' . escapeshellarg(implode(',', $contentTypeIdentifiers));
        $command .= ' --' . ExportContentTypesCommand::PARAM_EXCLUDE_ATTRIBUTES . '=' . escapeshellarg(implode(',', $excludeAttributes));
        $command .= ' --' . ExportContentTypesCommand::PARAM_ADMIN_LOGIN . '=' . escapeshellarg($adminLogin);
        $command .= ' --' . ExportContentTypesCommand::PARAM_PACKAGE_ID . '=' . escapeshellarg($packageId);
        $command .= '" > ' . $logFilepath . ' 2>&1 &';
        exec($command, $output, $returnCode);

        if ($returnCode !== 0) {
            $this->setCommandFailed(ExportContentTypesCommand::$COMMAND_NAME, $returnCode, $entity);
            return;
        }
    }

    public function exportContentTypes(
        string $adminLogin,
        string $packageId,
        array $contentTypeIdentifiers,
        array $excludeAttributes = [],
        $output = null
    ) {
        try {
            $this->output = $output;
            $this->setPackageId($packageId);
            $this->setAdminLogin($adminLogin);
            $this->setType(self::CONTENT_TYPES_TYPE);
            $this->writeln('<info>Exporting content type identifiers ' . implode(', ', $contentTypeIdentifiers) . '</info>');

            $entity = $this->clearAndGetStatus();
            $entity->setInProgress(true);
            $this->persistStatus($entity);

            $this->updateStatus($entity, 5);
            $this->writeln('<info>Setting up directory...</info>');
            $this->setupExportDirectories();

            $this->setupMigrateCommandArguments();

            $this->updateStatus($entity, 10);
            $this->writeln('<info>Generating migrations for Content Types...</info>');
            $this->generateMigrateFilesForContentTypes($contentTypeIdentifiers);

            $this->updateStatus($entity, 40);
            $this->removeIdsAndAttributesFromCreateMigrateFiles(
                $excludeAttributes,
                []
            );

            $this->updateStatus($entity, 50);
            // TODO: replace references just like we do in subtree export

            $this->updateStatus($entity, 70);
            $this->writeln('<info>Combining migrate files...</info>');
            $this->combineFiles();
            $this->updateStatus($entity, 80);

            $this->updateStatus($entity, 85);
            $this->writeln('<info>Creating metadata file...</info>');
            $delim = ',';
            $extraData = [
                ExportContentTypesCommand::PARAM_CONTENT_TYPE_IDENTIFIERS_LABEL => implode($delim, $contentTypeIdentifiers),
                ExportContentTypesCommand::PARAM_EXCLUDE_ATTRIBUTES_LABEL => implode($delim, $excludeAttributes),
            ];
            $this->createMetadataFile(
                $this->generatePackageName(),
                $adminLogin,
                $packageId,
                false,
                $extraData
            );

            $this->updateStatus($entity, 90);
            $this->writeln('<info>Creating zip...</info>');
            $this->createZip();

            $this->updateStatus($entity, 100);
            $entity->setInProgress(false);
            $this->persistStatus($entity);
            $this->writeln('');
            $this->writeln('<info>Done.</info>');
        } catch (Exception $e) {
            if (!isset($entity) || !($entity instanceof ContentPackageStatus)) {
                throw new Exception(self::ERROR_MESSAGES['entity'] . '(' . $e->getMessage() . ')');
            }

            $entity->setInProgress(false);
            $entity->setError($e->getMessage());
            $this->persistStatus($entity);
        }
    }

    protected function checkFullLogFilepath(
        $logFilepath
    ) {
        $fullLogFilepath = self::$PROJECT_ROOT_DIR . '/' . $logFilepath;
        if (!touch($fullLogFilepath)) {
            $entity->setInProgress(false);
            $entity->setError(
                str_replace('$FILE', $fullLogFilepath, self::ERROR_MESSAGES['log_write'])
            );
            $this->persistStatus($entity);

            throw new Exception();
        }
    }

    public function exportSubtreeAsync(
        ContentPackageStatus $entity,
        string $adminLogin,
        int $locationId,
        string $packageId,
        array $excludeAttributes = [],
        array $extraFieldsLocationId = [],
        array $extraFieldsContentId = [],
        array $addBracketsToFields = [],
        bool $includeFiles = true,
        bool $includeFileReferences = true,
        bool $preserveRemoteIds = false,
        bool $preserveOwner = true,
        bool $preservePublished = true,
        bool $preserveModified = true,
        bool $preserveSection = true,
        bool $preserveObjectStates = true,
        string $logFilepath = null
    ) {
        if ($logFilepath === null) {
            $logFilepath = $this->getLogFilepath(
                $adminLogin,
                $packageId,
                true,
                false
            );
        }
        try {
            $this->checkFullLogFilepath($logFilepath);
        } catch (Exception $e) {
            return;
        }

        $command = 'cd ' . self::$PROJECT_ROOT_DIR . ' && nohup sh -c "';
        $command .= self::$PHP_COMMAND . ' -dmemory_limit=-1 ';
        $command .= self::$CONSOLE_PATH . ' ' . ExportSubtreeCommand::COMMAND_NAME;
        $command .= ' --' . ExportSubtreeCommand::PARAM_SUBTREE_LOCATION_ID . '=' . escapeshellarg($locationId);
        $command .= ' --' . ExportSubtreeCommand::PARAM_EXCLUDE_ATTRIBUTES . '=' . escapeshellarg(implode(',', $excludeAttributes));
        $command .= ' --' . ExportSubtreeCommand::PARAM_EXTRA_FIELDS_LOCATION_ID . '=' . escapeshellarg(implode(',', $extraFieldsLocationId));
        $command .= ' --' . ExportSubtreeCommand::PARAM_EXTRA_FIELDS_CONTENT_ID . '=' . escapeshellarg(implode(',', $extraFieldsContentId));
        $command .= ' --' . ExportSubtreeCommand::PARAM_ADD_BRACKETS_TO_REFERENCE_FIELDS . '=' . escapeshellarg(implode(',', $addBracketsToFields));
        $command .= ' --' . ExportSubtreeCommand::PARAM_ADMIN_LOGIN . '=' . escapeshellarg($adminLogin);
        $command .= ' --' . ExportSubtreeCommand::PARAM_PACKAGE_ID . '=' . escapeshellarg($packageId);
        if ($includeFiles) {
            $command .= ' --' . ExportSubtreeCommand::PARAM_INCLUDE_FILES;
        }
        if ($includeFileReferences) {
            $command .= ' --' . ExportSubtreeCommand::PARAM_INCLUDE_FILE_REFERENCES;
        }
        if ($preserveRemoteIds) {
            $command .= ' --' . ExportSubtreeCommand::PARAM_PRESERVE_REMOTE_IDS;
        }
        if ($preserveOwner) {
            $command .= ' --' . ExportSubtreeCommand::PARAM_PRESERVE_OWNER;
        }
        if ($preservePublished) {
            $command .= ' --' . ExportSubtreeCommand::PARAM_PRESERVE_PUBLISHED;
        }
        if ($preserveModified) {
            $command .= ' --' . ExportSubtreeCommand::PARAM_PRESERVE_MODIFIED;
        }
        if ($preserveSection) {
            $command .= ' --' . ExportSubtreeCommand::PARAM_PRESERVE_SECTION;
        }
        if ($preserveObjectStates) {
            $command .= ' --' . ExportSubtreeCommand::PARAM_PRESERVE_OBJECT_STATES;
        }
        $command .= '" > ' . $logFilepath . ' 2>&1 &';
        exec($command, $output, $returnCode);

        if ($returnCode !== 0) {
            $this->setCommandFailed(ExportSubtreeCommand::COMMAND_NAME, $returnCode, $entity);
            return;
        }
    }

    public function exportSubtree(
        string $adminLogin,
        int $parentId,
        string $packageId,
        array $excludeAttributes = [],
        array $extraFieldsLocationId = [],
        array $extraFieldsContentId = [],
        array $addBracketsToFields = [],
        bool $includeFiles = true,
        bool $includeFileReferences = true,
        bool $preserveRemoteIds = false,
        bool $preserveOwner = true,
        bool $preservePublished = true,
        bool $preserveModified = true,
        bool $preserveSection = true,
        bool $preserveObjectStates = true,
        $output = null
    ) {
        try {
            $this->output = $output;
            $this->setPackageId($packageId);
            $this->setParentId($parentId);
            $this->setAdminLogin($adminLogin);
            $this->setType(self::SUBTREE_TYPE);

            $entity = $this->clearAndGetStatus();
            $entity->setInProgress(true);
            $this->persistStatus($entity);

            $this->writeln('<info>Exporting subtree location id ' . $this->getParentId() . '</info>');
            $this->parentLocation = $this->locationService->loadLocation($this->getParentId());

            $this->updateStatus($entity, 5);
            $this->writeln('<info>Setting up directory...</info>');
            $this->setupExportDirectories();

            $this->setupMigrateCommandArguments();

            $this->updateStatus($entity, 10);
            $this->writeln('<info>Generating migrations for locations...</info>');
            $this->generateMigrateFilesForLocations();

            $this->updateStatus($entity, 40);
            $this->writeln('<info>Removing ids in create migrate files...</info>');
            if (!$preserveRemoteIds) {
                $excludeAttributes[] = 'remote_id';
                $excludeAttributes[] = 'location_remote_id';
            }
            $excludeAttributes[] = 'new_remote_id';
            if (!$preserveOwner) {
                $excludeAttributes[] = 'owner';
            }
            if (!$preservePublished) {
                $excludeAttributes[] = 'publication_date';
            }
            if (!$preserveModified) {
                $excludeAttributes[] = 'modification_date';
            }
            if (!$preserveSection) {
                $excludeAttributes[] = 'section';
            }
            if (!$preserveObjectStates) {
                $excludeAttributes[] = 'object_states';
            }
            $excludeAttributesOnCreate = array_merge(
                $extraFieldsLocationId,
                $extraFieldsContentId
            );
            $this->removeIdsAndAttributesFromCreateMigrateFiles(
                $excludeAttributes,
                $excludeAttributesOnCreate
            );

            $this->updateStatus($entity, 50);
            $this->writeln('<info>Generating reference keys for ids...</info>');
            $parentParentLocationId = $this->parentLocation->parentLocationId;
            // this will be a placeholder so the import can be used under any subtree
            $extraReferences = [
                [
                    'type' => 'location_id',
                    'old_hardcoded_value' => $parentParentLocationId,
                    'new_reference_key' => self::getLocationReferenceKey(self::ROOT_PLACEHOLDER_ID),
                ]
            ];
            $this->generateReferenceKeys($extraReferences);

            $this->updateStatus($entity, 60);
            $this->writeln('<info>Replacing ids with references...</info>');
            $this->replaceReferences(
                $extraFieldsLocationId,
                $extraFieldsContentId,
                $addBracketsToFields
            );

            $this->updateStatus($entity, 70);
            $this->writeln('<info>Combining migrate files...</info>');
            $this->combineFiles();
            $this->updateStatus($entity, 80);
            if ($includeFiles) {
                $this->writeln('<info>Copying files...</info>');
                $this->copyFiles();
            } else {
                $this->writeln('<info>Skipping copying files...</info>');
            }

            if (!$includeFileReferences) {
                $this->writeln('<info>Removing file references...</info>');
                $this->removeFileReferences();
            }

            $this->updateStatus($entity, 85);
            $this->writeln('<info>Creating metadata file...</info>');
            $delim = ',';
            $extraData = [
                ExportSubtreeCommand::PARAM_SUBTREE_LOCATION_ID_LABEL => $this->getParentId(),
                ExportSubtreeCommand::PARAM_EXCLUDE_ATTRIBUTES_LABEL => implode($delim, $excludeAttributes),
                ExportSubtreeCommand::PARAM_EXTRA_FIELDS_LOCATION_ID_LABEL => implode($delim, $extraFieldsLocationId),
                ExportSubtreeCommand::PARAM_EXTRA_FIELDS_CONTENT_ID_LABEL => implode($delim, $extraFieldsContentId),
                ExportSubtreeCommand::PARAM_ADD_BRACKETS_TO_REFERENCE_FIELDS_LABEL => implode($delim, $addBracketsToFields),
                ExportSubtreeCommand::PARAM_INCLUDE_FILES_LABEL => $includeFiles,
                ExportSubtreeCommand::PARAM_INCLUDE_FILE_REFERENCES_LABEL => $includeFileReferences,
                ExportSubtreeCommand::PARAM_PRESERVE_REMOTE_IDS_LABEL => $preserveRemoteIds,
                ExportSubtreeCommand::PARAM_PRESERVE_OWNER_LABEL => $preserveOwner,
                ExportSubtreeCommand::PARAM_PRESERVE_PUBLISHED_LABEL => $preservePublished,
                ExportSubtreeCommand::PARAM_PRESERVE_MODIFIED_LABEL => $preserveModified,
                ExportSubtreeCommand::PARAM_PRESERVE_SECTION_LABEL => $preserveSection,
                ExportSubtreeCommand::PARAM_PRESERVE_OBJECT_STATES_LABEL => $preserveObjectStates,
            ];
            $this->createMetadataFile(
                $this->generatePackageName(),
                $adminLogin,
                $packageId,
                false,
                $extraData
            );

            $this->updateStatus($entity, 90);
            $this->writeln('<info>Creating zip...</info>');
            $this->createZip();

            $this->updateStatus($entity, 100);
            $entity->setInProgress(false);
            $this->persistStatus($entity);
            $this->writeln('');
            $this->writeln('<info>Done.</info>');
        } catch (Exception $e) {
            if (!isset($entity) || !($entity instanceof ContentPackageStatus)) {
                throw new Exception(self::ERROR_MESSAGES['entity'] . '(' . $e->getMessage() . ')');
            }

            $entity->setInProgress(false);
            $entity->setError($e->getMessage());
            $this->persistStatus($entity);
        }
    }

    protected function generatePackageName($params = [])
    {
        $name = '';
        $type = $this->getType();
        switch ($type) {
            case self::SUBTREE_TYPE:
                $name = 'Subtree Location "' . $this->parentLocation->contentInfo->name . '"';
                $name .= ' (id ' . $this->parentLocation->id . ')';
                break;
            case self::CONTENT_TYPES_TYPE:
                $name = 'Content Types';
                break;
            case self::TAGS_TYPE:
                $name = 'Tags Tree "' . ($params['keyword'] ?? '') . '" (id ' . $params['id']  . ')';
                break;
        }
        return $name;
    }

    public function setupExportDirectories(
        $adminLogin = null,
        $packageId = null,
        $noRootDir = false
    ) {
        $this->currentExportDirectoryName = $this->getUserPackageDirectory(
            $adminLogin,
            $packageId,
            $noRootDir,
            false
        );
        $this->ensureDirectoryExists($this->currentExportDirectoryName);
        $this->clearDirectory($this->currentExportDirectoryName, [], [self::LOG_FILENAME]);
        $it = new RecursiveDirectoryIterator(
            $this->currentExportDirectoryName,
            RecursiveDirectoryIterator::SKIP_DOTS
        );
        $filter = new MigrateFilesRecursiveFilterIterator($it);
        $this->currentExportDirectoryMigrateFilesIterator = new RecursiveIteratorIterator(
            $filter,
            RecursiveIteratorIterator::CHILD_FIRST
        );
        $filter = new NoDotRecursiveFilterIterator($it);
        $this->currentExportDirectoryIterator = new RecursiveIteratorIterator(
            $filter,
            RecursiveIteratorIterator::CHILD_FIRST
        );

        $filesDir = $this->getFilesDirectory(
            $adminLogin,
            $packageId,
            $noRootDir,
            false
        );
        $this->ensureDirectoryExists($filesDir);
        $this->clearDirectory($filesDir);
    }

    public function removeIdsAndAttributesFromCreateMigrateFiles(
        array $excludeAttributes = [],
        array $excludeAttributesOnCreate = []
    ) {
        foreach ($this->currentExportDirectoryMigrateFilesIterator as $fileInfo) {
            if (!$fileInfo->isFile()) {
                continue;
            }
            $fileName = $fileInfo->getFilename();
            $fullFilepath = $fileInfo->getPathname();
            $isCreate = (strpos($fileName, 'create') !== false);
            $emptyValues = $isCreate ? self::EMPTY_VALUES : [];
            $yaml = Yaml::parseFile($fullFilepath);
            foreach ($yaml as $k => $v) {
                $yaml[$k] = $this->removeIdsAndAttributes(
                    $k,
                    $v,
                    $emptyValues,
                    array_merge(
                        $excludeAttributes,
                        $isCreate ? $excludeAttributesOnCreate : []
                    )
                );
            }

            $yaml = Yaml::dump($yaml, 4, 4);
            file_put_contents($fullFilepath, $yaml);
        }
    }

    protected function getContentTypeIdentifierFromContentRemoteId($contentRemoteId)
    {
        $query = new Query();
        $query->limit = 1;
        $query->filter = new Criterion\LogicalAnd([
            new Criterion\RemoteId($contentRemoteId),
        ]);

        $searchResult = $this->searchService->findContentInfo($query);
        if ($searchResult->searchHits) {
            try {
                $contentTypeId = $searchResult->searchHits[0]->valueObject->contentTypeId;

                return $this->contentTypeService->loadContentType($contentTypeId)->identifier;
            } catch (Exception $e) {
                return null;
            }
        }

        return null;
    }

    protected function removeIdsAndAttributes(
        $key,
        $value,
        $emptyValues = [],
        $excludeAttributes = [],
        $contentTypeIdentifier = null
    ) {
        $isTag = false;
        if (!is_numeric($key) && isset($emptyValues[$key])) {
            $value = $emptyValues[$key];
        } elseif (is_array($value)) {
            if (isset($value['content_type'])) {
                $contentTypeIdentifier = $value['content_type'];
            } elseif (isset($value['match']['content_remote_id'])) {
                // we need to figure out the content type
                $contentRemoteId = $value['match']['content_remote_id'];
                $contentTypeIdentifier = $this->getContentTypeIdentifierFromContentRemoteId($contentRemoteId);
            }
            if (isset($value['main_tag_id']) && isset($value['keywords'])) {
                $isTag = true;
            }
            foreach ($value as $k => $v) {
                if ($isTag && $k != 'remote_id') {
                    unset($value[$k]);
                    continue;
                }
                if (
                    !is_numeric($k) &&
                    (
                        in_array($k, $excludeAttributes, true) ||
                        (
                            $contentTypeIdentifier !== null &&
                            in_array($contentTypeIdentifier . '/' . $k, $excludeAttributes, true)
                        )
                    )
                ) {
                    unset($value[$k]);
                    continue;
                }
                $value[$k] = $this->removeIdsAndAttributes(
                    $k,
                    $v,
                    $emptyValues,
                    $excludeAttributes,
                    $contentTypeIdentifier
                );
            }
        }

        return $value;
    }

    public function generateMigrateFilesForTags(
        string $parentTagId
    ) {
        $args = array_merge($this->baseArguments, [
            '--type' => 'tag',
            '--match-type' => 'tag_id',
        ]);

        $modes = [
            'create',
        ];

        $matchValues = [$parentTagId];
        $parentTagIds = [$parentTagId];
        do {
            $newParentTagIds = [];
            foreach ($parentTagIds as $thisParentTagId) {
                $thisOffset = 0;
                $thisLimit = self::BATCH_LIMIT;
                do {
                    $thisParentTag = $this->tagsService->loadTag($thisParentTagId);
                    $tags = $this->tagsService->loadTagChildren(
                        $thisParentTag,
                        $thisOffset,
                        $thisLimit
                    );
                    if (count($tags) === 0) {
                        break;
                    }

                    foreach ($tags as $tag) {
                        $matchValues[] = $tag->id;
                        $newParentTagIds[] = $tag->id;
                    }

                    $thisOffset += $thisLimit;
                } while (count($tags));
            }
            $parentTagIds = $newParentTagIds;
        } while (count($parentTagIds));

        $matchValueChunks = array_chunk($matchValues, self::BATCH_LIMIT);
        foreach ($matchValueChunks as $matchValueChunk) {
            foreach ($modes as $mode) {
                $arguments = array_merge($args, [
                    '--mode' => $mode,
                    '--match-value' => implode(',', $matchValueChunk),
                    'name' => $mode . '_tags',
                ]);
                $this->runGenerateCommand($arguments, self::TAGS_TYPE);
            }
        }
    }

    public function generateMigrateFilesForLocations()
    {
        if (!($this->parentLocation instanceof Location)) {
            throw new Exception(self::ERROR_MESSAGES['location_id']);
        }

        $contentArguments = array_merge($this->baseArguments, [
            '--type' => 'content',
            '--match-type' => 'content_id',
        ]);

        $modes = [
            'create',
            'update',
        ];

        $batchLimit = self::BATCH_LIMIT;
        $offset = 0;
        do {
            $locations = $this->getLocations(
                $this->parentLocation,
                $batchLimit,
                $offset,
                null,
                null
            );
            if (count($locations) === 0) {
                break;
            }

            $matchValues = [];
            foreach ($locations as $i => $location) {
                $locationId = $location->id;
                $contentId = $location->contentInfo->id;
                $contentTypeId = $location->contentInfo->contentTypeId;
                $this->contentTypeIds[] = $contentTypeId;
                $matchValues[] = $contentId;
                $this->contentMetadata[] = [
                    'location_id' => $locationId,
                    'content_id' => $contentId,
                    'content_type_id' => $contentTypeId,
                    'content_remote_id' => $location->contentInfo->remoteId,
                    'location_remote_id' => $location->remoteId,
                ];
            }
            foreach ($modes as $mode) {
                $arguments = array_merge($contentArguments, [
                    '--mode' => $mode,
                    '--match-value' => implode(',', $matchValues),
                    'name' => $mode . '_content',
                ]);
                $this->runGenerateCommand($arguments, self::SUBTREE_TYPE);
            }

            $offset += $batchLimit;
        } while (count($locations));
    }

    public function setupMigrateCommandArguments()
    {
        $this->baseArguments = [
            'command' => self::$GENERATE_COMMAND,
            'bundle' => $this->getUserPackageDirectory(
                null,
                null,
                true,
                false
            ),
            '--admin-login' => $this->getAdminLogin(),
            '--format' => 'yml',
        ];
    }

    public function getLocations(
        $parentLocation,
        $limit = 25,
        $offset = 0,
        $depth = null,
        $class = null
    ): array {
        $filters = [
            new Criterion\Subtree($parentLocation->pathString),
        ];

        if (isset($class)) {
            $filters[] = new Criterion\ContentTypeIdentifier($class);
        }

        if (isset($depth)) {
            $depth += count(array_filter(explode('/', $parentLocation->pathString))) - 1;
            $filters[] = new Criterion\Location\Depth(Criterion\Operator::EQ, $depth);
        }

        $query = new LocationQuery();
        $query->limit = $limit;
        $query->offset = $offset;
        $query->filter = new Criterion\LogicalAnd($filters);
        $query->sortClauses = [new SortClause\Location\Depth()];

        $searchResult = $this->searchService->findLocations($query);

        $locations = [];
        foreach ($searchResult->searchHits as $hit) {
            $locations[] = $hit->valueObject;
        }

        return $locations;
    }

    public function replaceReferences(
        array $extraFieldsLocationId = [],
        array $extraFieldsContentId = [],
        array $addBracketsToFields = []
    ) {
        // replace hardcoded ids with references

        foreach ($this->currentExportDirectoryMigrateFilesIterator as $fileInfo) {
            if (!$fileInfo->isFile()) {
                continue;
            }
            $fullFilepath = $fileInfo->getPathname();
            $yaml = Yaml::parseFile($fullFilepath);
            foreach ($this->references as $reference) {
                $type = $reference['type'];
                switch ($type) {
                    case 'location_id':
                        $keysToCheck = array_merge([
                            'parent_location',
                            'xml',
                        ], $extraFieldsLocationId);
                        break;
                    case 'content_id':
                        $keysToCheck = array_merge([
                            'destinationContentId',
                            'destinationContentIds',
                            'owner',
                            'xml',
                        ], $extraFieldsContentId);
                        break;
                    case 'content_remote_id':
                        $keysToCheck = [
                            'content_remote_id',
                        ];
                        break;
                    case 'content_type_id':
                        $keysToCheck = [
                            'content_type_id',
                        ];
                        break;
                    case 'tag_remote_id':
                        $keysToCheck = [
                            'parent_tag_id',
                            'remote_id',
                        ];
                        break;
                    default:
                        $keysToCheck = [];
                        break;
                }
                if (empty($keysToCheck)) {
                    continue;
                }

                foreach ($yaml as $k => $v) {
                    $yaml[$k] = self::replaceReferencesInner(
                        $k,
                        $v,
                        $keysToCheck,
                        $type,
                        $reference,
                        false,
                        $addBracketsToFields
                    );
                }
            }

            $yaml = Yaml::dump($yaml, 4, 4);
            file_put_contents($fullFilepath, $yaml);
        }
    }

    protected static function replaceReferencesInner(
        $key,
        $value,
        $keysToCheck,
        $type,
        $reference,
        $replace = false,
        $addBracketsToFields = [],
        $exactMatch = false,
        $noConvert = false
    ) {
        if (!is_numeric($key) && in_array($key, $keysToCheck, true)) {
            $replace = true;
        }
        $isNumber = is_int($value) || is_float($value);

        if (is_string($value) || $isNumber) {
            if ($replace) {
                $originalValue = $value;

                $addBrackets = in_array($key, $addBracketsToFields, true);

                $value = self::getNewValue(
                    $reference['old_hardcoded_value'],
                    $reference['new_reference_key'],
                    $value,
                    $key,
                    $type,
                    $addBrackets,
                    $exactMatch
                );
                if (!$noConvert && $isNumber && $value == $originalValue) {
                    $value = (int)$value;
                }
            }
        } elseif (is_array($value)) {
            foreach ($value as $k => $v) {
                if (!is_numeric($k) && in_array($k, $keysToCheck, true)) {
                    $replace = true;
                }
                $isTag = (isset($value['type']) && $value['type'] === 'tag');
                if ($isTag) {
                    $exactMatch = true;
                    $noConvert = true;
                }
                $value[$k] = self::replaceReferencesInner(
                    $k,
                    $v,
                    $keysToCheck,
                    $type,
                    $reference,
                    $replace,
                    $addBracketsToFields,
                    $exactMatch,
                    $noConvert
                );
            }
        }

        return $value;
    }

    protected static function getNewValue(
        string $old,
        string $new,
        string $str,
        $key,
        string $type,
        $addBrackets = false,
        $exactMatch = false
    ): string {
        if ($key === 'xml') {
            if ($type === 'location_id') {
                $regex = '/ezlocation\:\/\/' . $old . '/';
                $replace = 'ezlocation://[reference:' . $new . ']';
                $str = preg_replace($regex, $replace, $str);
                $regex = '/node_id="' . $old . '"/';
                $replace = 'node_id="[reference:' . $new . ']"';
                $str = preg_replace($regex, $replace, $str);
                $regex = '/eznode\:\/\/' . $old . '/';
                $replace = 'eznode://[reference:' . $new . ']';
                $str = preg_replace($regex, $replace, $str);

                return $str;
            }

            if ($type === 'content_id') {
                $regex = '/ezcontent\:\/\/' . $old . '/';
                $replace = 'ezcontent://[reference:' . $new . ']';
                $str = preg_replace($regex, $replace, $str);
                $regex = '/object_id="' . $old . '"/';
                $replace = 'object_id="[reference:' . $new . ']"';
                $str = preg_replace($regex, $replace, $str);

                return $str;
            }

            return $old;
        }

        if ($exactMatch) {
            if ($str === $old) {
                $str = 'reference:' . $new;
                if ($addBrackets) {
                    $str = '[' . $str . ']';
                }
                return $str;
            }
            return $str;
        }

        //$regex = '/\b' . $old . '\b/';
        $regex = '/(?<![0-9])' . $old . '(?![0-9])(?!_new)/';
        $replace = 'reference:' . $new;
        if ($addBrackets) {
            $replace = '[' . $replace . ']';
        }

        return preg_replace($regex, $replace, $str);
    }

    public function generateReferenceKeys(array $extraReferences = [])
    {
        $this->references = $extraReferences;
        $orderedFiles = $this->getOrderedFiles();
        foreach (self::REFERENCE_CONFIG as $config) {
            // we need to keep track of what metadata index we should check
            // since we generated the migrate files in batches
            // TODO: will probably have to rework this a bit once
            // we introduce other kinds of exports (content types, users, etc)
            $metadataIndex = 0;

            foreach ($orderedFiles as $orderedFile) {
                /** @var SplFileInfo $fileInfo */
                $fileInfo = $orderedFile['fileInfo'];
                if (!$fileInfo->isFile()) {
                    continue;
                }
                $fileName = $fileInfo->getFilename();
                if (strpos($fileName, 'create') === false) {
                    continue;
                }
                if (preg_match($config['file_regex'], $fileName, $matches) !== 1) {
                    continue;
                }
                $fullFilepath = $fileInfo->getPathname();

                $yaml = Yaml::parseFile($fullFilepath);

                foreach ($yaml as $key => $item) {
                    $referencesToAdd = [];
                    foreach ($config['references'] as $referenceSetting) {
                        $id = $this->contentMetadata[$metadataIndex][$referenceSetting['attribute']];
                        $callbackParam = $this->contentMetadata[$metadataIndex][$referenceSetting['callback_param']];
                        $identifier = call_user_func($referenceSetting['callback'], $callbackParam);
                        $referencesToAdd[] = [
                            'identifier' => $identifier,
                            'attribute' => $referenceSetting['attribute'],
                        ];

                        $this->references[] = [
                            'type' => $referenceSetting['attribute'],
                            'old_hardcoded_value' => $id,
                            'new_reference_key' => $identifier,
                        ];
                    }

                    if (count($referencesToAdd)) {
                        $yaml[$key]['references'] = [];
                        foreach ($referencesToAdd as $refToAdd) {
                            $yaml[$key]['references'][] = [
                                'identifier' => $refToAdd['identifier'],
                                'attribute' => $refToAdd['attribute'],
                            ];
                        }
                    }
                    ++$metadataIndex;
                }

                $yaml = Yaml::dump($yaml, 4, 4);
                file_put_contents($fullFilepath, $yaml);
            }
        }
    }

    protected function getOrderedFiles(
        array $skipFilepaths = [],
        array $skipFileNames = []
    ) {
        $skipFileNames[] = self::LOG_FILENAME;

        $orderedFiles = [];
        foreach ($this->currentExportDirectoryMigrateFilesIterator as $fileInfo) {
            if (!$fileInfo->isFile()) {
                continue;
            }
            $fileName = $fileInfo->getFilename();
            $fullFilepath = $fileInfo->getPathname();
            if (
                in_array($fullFilepath, $skipFilepaths, true) ||
                in_array($fileName, $skipFileNames, true)
            ) {
                continue;
            }
            preg_match('/.*?_(.*)/', $fileName, $matches);
            $withoutTimestamp = $matches[1];
            $orderedFiles[] = [
                'withoutTimestamp' => $withoutTimestamp,
                'fileName' => $fileName,
                'fullFilepath' => $fullFilepath,
                'fileInfo' => $fileInfo,
            ];
        }
        usort($orderedFiles, static function ($a, $b) {
            $aName = $a['withoutTimestamp'];
            $bName = $b['withoutTimestamp'];
            if ($aName === $bName) {
                $aFilename = $a['fileName'];
                $bFilename = $b['fileName'];

                return $aFilename < $bFilename ? -1 : 1;
            }

            return $aName < $bName ? -1 : 1;
        });

        return $orderedFiles;
    }

    public function combineFiles($type = null)
    {
        $fullFinalFilepath = $this->getMigrationFilepath(
            null,
            null,
            false,
            false,
            $type
        );
        $finalFp = fopen($fullFinalFilepath, 'ab');
        $orderedFiles = $this->getOrderedFiles(
            [$fullFinalFilepath,]
        );

        foreach ($orderedFiles as $file) {
            $content = file_get_contents($file['fullFilepath']);
            $content = '# ' . $file['fileName'] . "\r\n" . $content . "\r\n";
            fwrite($finalFp, $content);
            unlink($file['fullFilepath']);
        }
        fclose($finalFp);
    }

    protected function copyFiles()
    {
        $filepath = $this->getMigrationFilepath(
            null,
            null,
            false,
            false,
            null
        );
        $filesDir = $this->getFilesDirectory(
            null,
            null,
            false,
            false
        );

        $handle = fopen($filepath, 'rb');
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                // TODO: let user pass in extra binary keys,
                // to deal with migrate files' "simplified form" of
                // "attr_identifier: /path/to/file"
                // but I don't think GenerateCommand will ever make those
                $binaryKeys = self::BINARY_KEYS;
                foreach ($binaryKeys as $binaryKey) {
                    $trimmedLine = trim($line);
                    if (strpos($trimmedLine, $binaryKey) === 0) {
                        $parts = explode($binaryKey, $line, 2);
                        if (count($parts) < 2 || empty(trim($parts[1]))) {
                            continue;
                        }
                        $path = trim($parts[1]);
                        $path = ltrim($path, '\'"');
                        $path = rtrim($path, '\'"');
                        if (!$path || $path === 'null' || $path === 'false') {
                            continue;
                        }
                        if (!file_exists($path)) {
                            $this->writeln('<error>Binary filepath ' . $path . ' does not exist</error>');
                            continue;
                        }
                        $noSlashPath = ltrim($path, '/');
                        $localPath = $filesDir . '/' . $noSlashPath;
                        $this->ensureDirectoryExists($localPath, null, 1);
                        if (file_exists($localPath)) {
                            $this->writeln('<error>Local directory for ' . $localPath . ' does not exist</error>');
                            continue;
                        }
                        copy($path, $localPath);
                    }
                }
            }
            fclose($handle);
        } else {
            throw new Exception(
                str_replace(
                    '$FILEPATH',
                    $filepath,
                    self::ERROR_MESSAGES['file_open']
                )
            );
        }
    }

    protected function removeFileReferences()
    {
        $filepath = $this->getMigrationFilepath(
            null,
            null,
            false,
            false,
            null
        );
        $reading = fopen($filepath, 'rb');
        if ($reading === false) {
            throw new Exception(
                str_replace(
                    '$FILEPATH',
                    $reading,
                    self::ERROR_MESSAGES['file_open']
                )
            );
        }
        $filepathTemp = $filepath . '.tmp';
        $writing = fopen($filepathTemp, 'wb');
        if ($writing === false) {
            throw new Exception(
                str_replace(
                    '$FILEPATH',
                    $filepathTemp,
                    self::ERROR_MESSAGES['file_open']
                )
            );
        }

        $replaced = false;
        $binaryKeys = self::BINARY_KEYS;
        while (!feof($reading)) {
            $line = fgets($reading);
            foreach ($binaryKeys as $binaryKey) {
                $trimmedLine = trim($line);
                if (strpos($trimmedLine, $binaryKey) === 0) {
                    $parts = explode($binaryKey, $line, 2);
                    if (count($parts) < 2 || empty(trim($parts[1]))) {
                        continue;
                    }
                    $path = trim($parts[1]);
                    $path = ltrim($path, '\'"');
                    $path = rtrim($path, '\'"');
                    if (!$path || $path === 'null' || $path === 'false') {
                        continue;
                    }
                    $parts[1] = " ~\r\n";
                    $line = implode($binaryKey, $parts);
                    $replaced = true;
                }
            }
            fwrite($writing, $line);
        }
        fclose($reading);
        fclose($writing);

        if ($replaced) {
            rename($filepathTemp, $filepath);
        } else {
            unlink($filepathTemp);
        }
    }

    protected function createZip()
    {
        $zipPath = $this->getZipFilepath(
            null,
            null,
            false,
            false
        );
        $zip = new ZipArchive();
        $zip->open($zipPath, ZipArchive::CREATE | ZipArchive::OVERWRITE);

        foreach ($this->currentExportDirectoryIterator as $fileInfo) {
            if (!$fileInfo->isFile()) {
                continue;
            }
            $fileName = $fileInfo->getFilename();
            if ($fileName === self::LOG_FILENAME) {
                continue;
            }
            $fullFilepath = $fileInfo->getPathname();
            $relativePath = substr($fullFilepath, strlen($this->currentExportDirectoryName) + 1);
            $zip->addFile($fullFilepath, $relativePath);
        }

        $zip->close();

        $this->clearDirectory(
            $this->currentExportDirectoryName,
            [],
            [
                $this->getZipFilename(null),
                self::LOG_FILENAME,
            ]
        );
    }

    public function getMigrationFilepath(
        $adminLogin = null,
        $packageId = null,
        $noRootDir = false,
        $import = false,
        $type = null
    ) {
        if ($type === null) {
            $type = $this->getType();
        }
        if ($adminLogin === null) {
            $adminLogin = $this->getAdminLogin();
        }
        if ($packageId === null) {
            $packageId = $this->getPackageId();
        }
        $d = $this->getUserPackageDirectory($adminLogin, $packageId, $noRootDir, $import);
        $d .= '/' . $type . '_' . $packageId . '.yml';

        return $d;
    }

    public function generateMigrateFilesForContentTypes(
        array $contentTypeIdentifiers
    ) {
        $contentArguments = array_merge($this->baseArguments, [
            '--type' => 'content_type',
            '--match-type' => 'contenttype_identifier',
        ]);

        $modes = [
            'create',
        ];

        $matchValues = [];
        foreach ($contentTypeIdentifiers as $contentTypeIdentifier) {
            try {
                $this->contentTypeService->loadContentTypeByIdentifier($contentTypeIdentifier);
            } catch (Exception $e) {
                $this->writeln('<error>Content type identifier "' . $contentTypeIdentifier . '" not found.</error>');
                continue;
            }
            $matchValues[] = $contentTypeIdentifier;
        }

        foreach ($modes as $mode) {
            $arguments = array_merge($contentArguments, [
                '--mode' => $mode,
                '--match-value' => implode(',', $matchValues),
                'name' => $mode . '_content-types',
            ]);
            $this->runGenerateCommand($arguments, self::CONTENT_TYPES_TYPE);
        }
    }

    protected function runGenerateCommand(
        array $arguments,
        string $mode
    ) {
        $this->writeln('Running command ' . self::$GENERATE_COMMAND);
        $this->writeln('Params: ' . print_r($arguments, true));

        $application = new Application($this->kernel);
        $application->setAutoExit(false);
        $input = new ArrayInput($arguments);
        $output = new BufferedOutput();
        try {
            $returnCode = $application->run($input, $output);
        } catch (Exception $e) {
            throw new Exception(
                $this->getCommandFailedError(self::$GENERATE_COMMAND, $e->getCode()) .
                ' (mode "' . $mode . '"): ' . $e->getMessage()
            );
        }
        // sleep to guarantee a new migrate file timestamp
        sleep(1);

        $content = $output->fetch();
        $this->writeln(print_r($content, true));

        if ($returnCode !== 0) {
            throw new Exception(
                $this->getCommandFailedError(self::$GENERATE_COMMAND, $returnCode) .
                ' (mode "' . $mode . '")'
            );
        }
    }
}
