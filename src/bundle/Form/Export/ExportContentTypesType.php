<?php

namespace ContextualCode\EzPlatformContentPackagesBundle\Form\Export;

use ContextualCode\EzPlatformContentPackagesBundle\Command\ExportContentTypesCommand;
use eZ\Publish\API\Repository\ContentTypeService;
use eZ\Publish\Core\MVC\ConfigResolverInterface;
use eZ\Publish\Core\MVC\Symfony\RequestStackAware;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class ExportContentTypesType extends AbstractType
{
    use RequestStackAware;

    // see constructor
    protected $PARAM_DEFAULTS = [];

    /** @var ConfigResolverInterface */
    protected $configResolver;

    /** @var ContentTypeService */
    protected $contentTypeService;

    /** @var array */
    protected $contentTypeIdentifierToGroup = [];

    public function __construct(
        ConfigResolverInterface $configResolver,
        ContentTypeService $contentTypeService,
        $config
    ) {
        $this->configResolver = $configResolver;
        $this->contentTypeService = $contentTypeService;
        $this->PARAM_DEFAULTS = [
            ExportContentTypesCommand::PARAM_CONTENT_TYPE_IDENTIFIERS => [],
            ExportContentTypesCommand::PARAM_EXCLUDE_ATTRIBUTES => '',
        ];
        $params = $config['default_export_content_types_params'] ?? [];
        foreach ($params as $key => $param) {
            $params[str_replace('_', '-', $key)] = $param;
        }
        $this->PARAM_DEFAULTS = array_merge(
            $this->PARAM_DEFAULTS,
            $params
        );
    }

    protected function getData()
    {
        $request = $this->getCurrentRequest();

        return array_merge($this->PARAM_DEFAULTS, $request->query->all());
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);

        $form->setData($this->getData());
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(ExportContentTypesCommand::PARAM_CONTENT_TYPE_IDENTIFIERS, ChoiceType::class, [
                'label' => ExportContentTypesCommand::PARAM_CONTENT_TYPE_IDENTIFIERS_LABEL,
                'required' => true,
                'multiple' => true,
                'expanded' => false,
                'group_by' => function ($choice, $key, $value) {
                    return $this->contentTypeIdentifierToGroup[$value];
                },
                'choices' => $this->loadContentTypes(),
            ])
            ->add(ExportContentTypesCommand::PARAM_EXCLUDE_ATTRIBUTES, TextType::class, [
                'label' => ExportContentTypesCommand::PARAM_EXCLUDE_ATTRIBUTES_LABEL,
                'required' => false,
            ])
            ->add('generate', SubmitType::class, [
                'label' => 'Export Content Package',
                'attr' => ['class' => 'btn btn-primary'],
            ])
            ->setMethod('GET');
    }

    protected function loadContentTypes()
    {
        $classes = [];
        $contentTypeGroups = $this->contentTypeService->loadContentTypeGroups();
        foreach ($contentTypeGroups as $contentTypeGroup) {
            $contentTypes = $this->contentTypeService->loadContentTypes($contentTypeGroup);
            foreach ($contentTypes as $contentType) {
                $classes[$contentType->getName()] = $contentType->identifier;
                $this->contentTypeIdentifierToGroup[$contentType->identifier] = $contentTypeGroup->identifier;
            }
        }

        return $classes;
    }
}
