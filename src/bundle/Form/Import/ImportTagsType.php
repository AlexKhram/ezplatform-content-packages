<?php

namespace ContextualCode\EzPlatformContentPackagesBundle\Form\Import;

use ContextualCode\EzPlatformContentPackagesBundle\Command\ImportCommand;
use eZ\Publish\Core\MVC\Symfony\RequestStackAware;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;

class ImportTagsType extends AbstractType
{
    use RequestStackAware;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(ImportSubtreeType::PARAM_FILE, FileType::class, [
                'label' => 'Content Package File:',
                'required' => true,
                'constraints' => [
                    new File([
                        'mimeTypes' => [
                            'application/zip',
                            'application/octet-stream',
                            'application/x-zip-compressed',
                            'multipart/x-zip',
                            'application/x-gzip',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid Content Package zip file',
                    ]),
                ],
            ])
            ->add(ImportCommand::PARAM_PARENT_ID, IntegerType::class, [
                'label' => false,
                'required' => true,
            ])
            ->add('generate', SubmitType::class, [
                'label' => 'Preview Content Package Import',
                'attr' => ['class' => 'btn btn-primary'],
            ])
            ->setMethod('POST');
    }
}
