<?php

namespace ContextualCode\EzPlatformContentPackagesBundle\Form\Import;

use ContextualCode\EzPlatformContentPackagesBundle\Command\ImportCommand;
use eZ\Publish\Core\MVC\ConfigResolverInterface;
use eZ\Publish\Core\MVC\Symfony\RequestStackAware;
use EzSystems\EzPlatformAdminUiBundle\Templating\Twig\UniversalDiscoveryExtension;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;

class ImportSubtreeType extends AbstractType
{
    use RequestStackAware;

    public const PARAM_FILE = 'file';

    /** @var ConfigResolverInterface */
    protected $configResolver;

    /** @var UniversalDiscoveryExtension */
    protected $udwExtension;

    public function __construct(
        ConfigResolverInterface $configResolver,
        UniversalDiscoveryExtension $udwExtension
    ) {
        $this->configResolver = $configResolver;
        $this->udwExtension = $udwExtension;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('browse', ButtonType::class, [
                'label' => 'Browse',
                'attr' => [
                    'class' => 'btn--udw-browse-custom btn btn-secondary',
                    'data-udw-config' => $this->udwExtension->renderUniversalDiscoveryWidgetConfig('single', [
                        'type' => 'object_relation',
                        'starting_location_id' => 1,
                    ]),
                    'data-starting-location-id' => $this->configResolver->getParameter(
                        'universal_discovery_widget_module.default_location_id'
                    ),
                ],
            ])
            ->add(ImportCommand::PARAM_PARENT_ID, IntegerType::class, [
                'label' => false,
                'required' => true,
            ])
            ->add(self::PARAM_FILE, FileType::class, [
                'label' => 'Content Package File:',
                'required' => true,
                'constraints' => [
                    new File([
                        'mimeTypes' => [
                            'application/zip',
                            'application/octet-stream',
                            'application/x-zip-compressed',
                            'multipart/x-zip',
                            'application/x-gzip',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid Content Package zip file',
                    ]),
                ],
            ])
            ->add('generate', SubmitType::class, [
                'label' => 'Preview Content Package Import',
                'attr' => ['class' => 'btn btn-primary'],
            ])
            ->setMethod('POST');
    }
}
