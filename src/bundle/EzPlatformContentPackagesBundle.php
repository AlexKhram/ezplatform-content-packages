<?php

namespace ContextualCode\EzPlatformContentPackagesBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class EzPlatformContentPackagesBundle extends Bundle
{
    protected $name = 'EzPlatformContentPackagesBundle';
}
