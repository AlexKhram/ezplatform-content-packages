<?php

declare(strict_types=1);

namespace ContextualCode\EzPlatformContentPackagesBundle\DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20191020184132 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql(
            'CREATE TABLE `cc_content_packages_status` (
                  `package_id` varchar(255) NOT NULL,
                  `user_id` varchar(255) NOT NULL,
                  `timestamp` int(255) unsigned,
                  `in_progress` boolean,
                  `progress` int(11) unsigned NOT NULL,
                  `error` varchar(255) NOT NULL,
                  PRIMARY KEY (`package_id`, `user_id`)
                ) ENGINE = InnoDB DEFAULT CHARSET = utf8;'
        );
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE cc_content_packages_status');
    }
}
