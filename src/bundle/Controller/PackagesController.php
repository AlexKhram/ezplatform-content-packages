<?php

namespace ContextualCode\EzPlatformContentPackagesBundle\Controller;

use ContextualCode\EzPlatformContentPackagesBundle\Command\ExportContentTypesCommand;
use ContextualCode\EzPlatformContentPackagesBundle\Command\ExportSubtreeCommand;
use ContextualCode\EzPlatformContentPackagesBundle\Command\ExportTagsCommand;
use ContextualCode\EzPlatformContentPackagesBundle\Command\ImportCommand;
use ContextualCode\EzPlatformContentPackagesBundle\Form\Export\ExportContentTypesType;
use ContextualCode\EzPlatformContentPackagesBundle\Form\Export\ExportSubtreeType;
use ContextualCode\EzPlatformContentPackagesBundle\Form\Export\ExportTagsType;
use ContextualCode\EzPlatformContentPackagesBundle\Form\Import\ImportContentTypesType;
use ContextualCode\EzPlatformContentPackagesBundle\Form\Import\ImportSubtreeType;
use ContextualCode\EzPlatformContentPackagesBundle\Form\Import\ImportTagsType;
use ContextualCode\EzPlatformContentPackagesBundle\Service\ContentPackagesExportService;
use ContextualCode\EzPlatformContentPackagesBundle\Service\ContentPackagesImportService;
use ContextualCode\EzPlatformContentPackagesBundle\Service\ContentPackagesService;
use Exception;
use eZ\Bundle\EzPublishCoreBundle\Controller as BaseController;
use eZ\Publish\API\Repository\LocationService;
use eZ\Publish\API\Repository\PermissionResolver;
use eZ\Publish\API\Repository\Values\Content\Location;
use eZ\Publish\Core\MVC\Symfony\RequestStackAware;
use eZ\Publish\Core\MVC\Symfony\SiteAccess;
use eZ\Publish\Core\Repository\UserService;
use EzSystems\EzPlatformAdminUi\Notification\NotificationHandlerInterface;
use Netgen\TagsBundle\Core\Repository\TagsService;
use Netgen\TagsBundle\API\Repository\Values\Tags\Tag;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\RouterInterface;

class PackagesController extends BaseController
{
    use RequestStackAware;

    protected const ERROR_MESSAGES = [
        'invalid_form' => 'Form was invalid.',
        'no_file' => 'The file \'$FILE\' was not found.',
        'form_error' => 'Form error: $ERROR',
        'parent_id' => 'Please select a valid parent.',
        'parent_location' => 'Please select a valid parent location.',
        'parent_tag' => 'Please enter a valid parent tag id.',
        'select_file' => 'Please select a file.',
        'type' => 'That import/export type does not exist.',
    ];

    /** @var RouterInterface $router */
    protected $router;

    /** @var SiteAccess $siteAccess */
    protected $siteAccess;

    /** @var PermissionResolver */
    protected $permissionResolver;

    /** @var UserService */
    protected $userService;

    /** @var ContentPackagesImportService */
    protected $importService;

    /** @var ContentPackagesExportService */
    protected $exportService;

    /** @var NotificationHandlerInterface */
    protected $notificationHandler;

    /** @var LocationService */
    protected $locationService;

    /** @var TagsService */
    protected $tagsService;

    public function __construct(
        RouterInterface $router,
        SiteAccess $siteAccess,
        PermissionResolver $permissionResolver,
        UserService $userService,
        ContentPackagesImportService $contentPackagesImportService,
        ContentPackagesExportService $contentPackagesExportService,
        NotificationHandlerInterface $notificationHandler,
        LocationService $locationService,
        TagsService $tagsService
    ) {
        $this->router = $router;
        $this->siteAccess = $siteAccess;
        $this->permissionResolver = $permissionResolver;
        $this->userService = $userService;
        $this->importService = $contentPackagesImportService;
        $this->exportService = $contentPackagesExportService;
        $this->notificationHandler = $notificationHandler;
        $this->locationService = $locationService;
        $this->tagsService = $tagsService;
    }

    /**
     * Ensures that only authenticated users can access controller.
     */
    public function performAccessChecks()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
    }

    public function showDashboardAction()
    {
        $this->performAccessChecks();

        return $this->render('@ezdesign/content_packages/dashboard.html.twig');
    }

    public function exportAction()
    {
        $this->performAccessChecks();

        return $this->render('@ezdesign/content_packages/export/export.html.twig', [
            'types' => $this->exportService->getEnabledTypes(),
        ]);
    }

    public function exportSubtreeAction()
    {
        $this->performAccessChecks();

        try {
            $form = $this->getExportSubtreeForm();
            $formView = $form->createView();
        } catch (Exception $e) {
            $this->notificationHandler->error(
                str_replace(
                    '$ERROR',
                    $e->getMessage(),
                    self::ERROR_MESSAGES['form_error']
                )
            );
            $url = $this->router->generate(
                'contextual_code_content_packages_export',
                [
                    'siteaccess' => $this->siteAccess->name,
                ]
            );

            return new RedirectResponse($url);
        }

        $params = [
            'form' => $form,
            'form_view' => $formView,
        ];

        return $this->render(
            '@ezdesign/content_packages/export/export_subtree.html.twig',
            $params
        );
    }

    public function exportTagsAction()
    {
        $this->performAccessChecks();

        try {
            $form = $this->getExportTagsForm();
            $formView = $form->createView();
        } catch (Exception $e) {
            $this->notificationHandler->error(
                str_replace(
                    '$ERROR',
                    $e->getMessage(),
                    self::ERROR_MESSAGES['form_error']
                )
            );
            $url = $this->router->generate(
                'contextual_code_content_packages_export',
                [
                    'siteaccess' => $this->siteAccess->name,
                ]
            );

            return new RedirectResponse($url);
        }

        $params = [
            'form' => $form,
            'form_view' => $formView,
        ];

        return $this->render(
            '@ezdesign/content_packages/export/export_tags.html.twig',
            $params
        );
    }

    public function exportContentTypesAction()
    {
        $this->performAccessChecks();

        try {
            $form = $this->getExportContentTypesForm();
            $formView = $form->createView();
        } catch (Exception $e) {
            $this->notificationHandler->error(
                str_replace(
                    '$ERROR',
                    $e->getMessage(),
                    self::ERROR_MESSAGES['form_error']
                )
            );
            $url = $this->router->generate(
                'contextual_code_content_packages_export',
                [
                    'siteaccess' => $this->siteAccess->name,
                ]
            );

            return new RedirectResponse($url);
        }

        $params = [
            'form' => $form,
            'form_view' => $formView,
        ];

        return $this->render(
            '@ezdesign/content_packages/export/export_content_types.html.twig',
            $params
        );
    }

    public function exportContentTypesSubmitAction(Request $request)
    {
        $this->performAccessChecks();

        try {
            $form = $this->getExportContentTypesForm();
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $formData = $form->getData();
            } else {
                throw new Exception($this->getFormErrorMessage($form));
            }

            $adminLogin = $this->getUserLogin();
            $contentTypeIdentifiers = $formData[ExportContentTypesCommand::PARAM_CONTENT_TYPE_IDENTIFIERS] ?? [];
            $excludeAttributes = $formData[ExportContentTypesCommand::PARAM_EXCLUDE_ATTRIBUTES] ?? '';
            $excludeAttributes = explode(',', $excludeAttributes);

            // clear progress manually
            $packageId = time();
            $entity = $this->exportService->clearAndGetStatus($adminLogin, $packageId);
            $entity->setInProgress(true);
            $this->exportService->persistStatus($entity);
            $this->exportService->setupExportDirectories($adminLogin, $packageId);

            // start export
            $this->exportService->exportContentTypesAsync(
                $entity,
                $adminLogin,
                $packageId,
                $contentTypeIdentifiers,
                $excludeAttributes,
                null
            );

            if (!empty($entity->getError())) {
                $error = $entity->getError();
                $this->exportService->removeStatus($entity);
                throw new Exception($error);
            }

            $url = $this->router->generate(
                'contextual_code_content_packages_export_view',
                [
                    'siteaccess' => $this->siteAccess->name,
                    'packageId' => $packageId,
                ]
            );

            return new RedirectResponse($url);
        } catch (Exception $e) {
            $this->notificationHandler->error($e->getMessage());
            $url = $this->router->generate(
                'contextual_code_content_packages_export_content_types',
                [
                    'siteaccess' => $this->siteAccess->name,
                ]
            );

            return new RedirectResponse($url);
        }
    }

    public function exportTagsSubmitAction(Request $request)
    {
        $this->performAccessChecks();

        try {
            $form = $this->getExportTagsForm();
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $formData = $form->getData();
            } else {
                throw new Exception($this->getFormErrorMessage($form));
            }

            $adminLogin = $this->getUserLogin();
            $parentTagId = $formData[ExportTagsCommand::PARAM_PARENT_TAG_ID] ?? -1;

            // clear progress manually
            $packageId = time();
            $entity = $this->exportService->clearAndGetStatus($adminLogin, $packageId);
            $entity->setInProgress(true);
            $this->exportService->persistStatus($entity);
            $this->exportService->setupExportDirectories($adminLogin, $packageId);

            // start export
            $this->exportService->exportTagsAsync(
                $entity,
                $adminLogin,
                $packageId,
                $parentTagId,
                null
            );

            if (!empty($entity->getError())) {
                $error = $entity->getError();
                try {
                    $this->exportService->removeStatus($entity);
                } catch (Exception $e) {
                }
                throw new Exception($error);
            }

            $url = $this->router->generate(
                'contextual_code_content_packages_export_view',
                [
                    'siteaccess' => $this->siteAccess->name,
                    'packageId' => $packageId,
                ]
            );

            return new RedirectResponse($url);
        } catch (Exception $e) {
            $this->notificationHandler->error($e->getMessage());
            $url = $this->router->generate(
                'contextual_code_content_packages_export_tags',
                [
                    'siteaccess' => $this->siteAccess->name,
                ]
            );

            return new RedirectResponse($url);
        }
    }

    public function exportSubtreeSubmitAction(Request $request)
    {
        $this->performAccessChecks();

        try {
            $form = $this->getExportSubtreeForm();
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $formData = $form->getData();
            } else {
                throw new Exception($this->getFormErrorMessage($form));
            }

            $adminLogin = $this->getUserLogin();
            $locationId = (int)$formData[ExportSubtreeCommand::PARAM_SUBTREE_LOCATION_ID];
            if (!$locationId) {
                throw new Exception(self::ERROR_MESSAGES['parent_id']);
            }
            $this->locationService->loadLocation($locationId); // throws exception

            $excludeAttributes = $formData[ExportSubtreeCommand::PARAM_EXCLUDE_ATTRIBUTES] ?? '';
            $excludeAttributes = explode(',', $excludeAttributes);
            $extraFieldsLocationId = $formData[ExportSubtreeCommand::PARAM_EXTRA_FIELDS_LOCATION_ID] ?? '';
            $extraFieldsLocationId = explode(',', $extraFieldsLocationId);
            $extraFieldsContentId = $formData[ExportSubtreeCommand::PARAM_EXTRA_FIELDS_CONTENT_ID] ?? '';
            $extraFieldsContentId = explode(',', $extraFieldsContentId);
            $addBracketsToFields = $formData[ExportSubtreeCommand::PARAM_ADD_BRACKETS_TO_REFERENCE_FIELDS] ?? '';
            $addBracketsToFields = explode(',', $addBracketsToFields);
            $includeFileBinaries = (bool)$formData[ExportSubtreeCommand::PARAM_INCLUDE_FILES];
            $includeFileReferences = (bool)$formData[ExportSubtreeCommand::PARAM_INCLUDE_FILE_REFERENCES];
            $preserveRemoteIds = (bool)$formData[ExportSubtreeCommand::PARAM_PRESERVE_REMOTE_IDS];
            $preserveOwner = (bool)$formData[ExportSubtreeCommand::PARAM_PRESERVE_OWNER];
            $preservePublished = (bool)$formData[ExportSubtreeCommand::PARAM_PRESERVE_PUBLISHED];
            $preserveModified = (bool)$formData[ExportSubtreeCommand::PARAM_PRESERVE_MODIFIED];
            $preserveSection = (bool)$formData[ExportSubtreeCommand::PARAM_PRESERVE_SECTION];
            $preserveObjectStates = (bool)$formData[ExportSubtreeCommand::PARAM_PRESERVE_OBJECT_STATES];

            // clear progress manually
            $packageId = time();
            $entity = $this->exportService->clearAndGetStatus($adminLogin, $packageId);
            $entity->setInProgress(true);
            $this->exportService->persistStatus($entity);
            $this->exportService->setupExportDirectories($adminLogin, $packageId);

            // start export
            $this->exportService->exportSubtreeAsync(
                $entity,
                $adminLogin,
                $locationId,
                $packageId,
                $excludeAttributes,
                $extraFieldsLocationId,
                $extraFieldsContentId,
                $addBracketsToFields,
                $includeFileBinaries,
                $includeFileReferences,
                $preserveRemoteIds,
                $preserveOwner,
                $preservePublished,
                $preserveModified,
                $preserveSection,
                $preserveObjectStates,
                null
            );

            if (!empty($entity->getError())) {
                $error = $entity->getError();
                $this->exportService->removeStatus($entity);
                throw new Exception($error);
            }

            $url = $this->router->generate(
                'contextual_code_content_packages_export_view',
                [
                    'siteaccess' => $this->siteAccess->name,
                    'packageId' => $packageId,
                ]
            );

            return new RedirectResponse($url);
        } catch (Exception $e) {
            $this->notificationHandler->error($e->getMessage());
            $url = $this->router->generate(
                'contextual_code_content_packages_export_subtree',
                [
                    'siteaccess' => $this->siteAccess->name,
                ]
            );

            return new RedirectResponse($url);
        }
    }

    protected function getUserLogin()
    {
        $userReference = $this->permissionResolver->getCurrentUserReference();

        return $this->userService->loadUser($userReference->getUserId())->login;
    }

    protected function getExportSubtreeForm()
    {
        return $this->createForm(ExportSubtreeType::class, null, [
            'action' => $this->generateUrl('contextual_code_content_packages_export_subtree_submit'),
            'method' => 'GET',
        ]);
    }

    protected function getExportTagsForm()
    {
        return $this->createForm(ExportTagsType::class, null, [
            'action' => $this->generateUrl('contextual_code_content_packages_export_tags_submit'),
            'method' => 'GET',
        ]);
    }

    protected function getExportContentTypesForm()
    {
        return $this->createForm(ExportContentTypesType::class, null, [
            'action' => $this->generateUrl('contextual_code_content_packages_export_content_types_submit'),
            'method' => 'GET',
        ]);
    }

    protected function getImportSubtreeForm()
    {
        return $this->createForm(ImportSubtreeType::class, null, [
            'action' => $this->generateUrl('contextual_code_content_packages_import_submit'),
            'method' => 'GET',
        ]);
    }

    protected function getImportContentTypesForm()
    {
        return $this->createForm(ImportContentTypesType::class, null, [
            'action' => $this->generateUrl(
                'contextual_code_content_packages_import_submit'
            ),
            'method' => 'GET',
        ]);
    }

    protected function getImportTagsForm()
    {
        return $this->createForm(ImportTagsType::class, null, [
            'action' => $this->generateUrl(
                'contextual_code_content_packages_import_submit'
            ),
            'method' => 'GET',
        ]);
    }

    public function exportViewAction($packageId)
    {
        $this->performAccessChecks();

        try {
            $adminLogin = $this->getUserLogin();
            $entity = $this->exportService->getStatus($adminLogin, $packageId);
            $statusLog = $this->exportService->getStatusLog(
                $adminLogin,
                $packageId,
                false,
                false
            );

            return $this->render(
                '@ezdesign/content_packages/export/export_progress.html.twig',
                [
                    'status_log' => $statusLog,
                    'package_id' => $packageId,
                    'entity' => $entity,
                ]
            );
        } catch (Exception $e) {
            $this->notificationHandler->error($e->getMessage());
            $url = $this->router->generate(
                'contextual_code_content_packages_export_view',
                [
                    'siteaccess' => $this->siteAccess->name,
                    'packageId' => $packageId,
                ]
            );

            return new RedirectResponse($url);
        }
    }

    public function downloadAction($packageId)
    {
        $this->performAccessChecks();

        try {
            $adminLogin = $this->getUserLogin();
            $fullFilepath = $this->exportService->getZipFilepath($adminLogin, $packageId);
            if (!file_exists($fullFilepath)) {
                throw new Exception(
                    str_replace('$FILE', $fullFilepath, self::ERROR_MESSAGES['no_file'])
                );
            }
            $basename = basename($fullFilepath);
            $response = new BinaryFileResponse($fullFilepath);
            $response->headers->set('Content-Type', 'application/zip');
            $response->setContentDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                $basename
            );

            return $response;
        } catch (Exception $e) {
            $this->notificationHandler->error($e->getMessage());
            $url = $this->router->generate(
                'contextual_code_content_packages_export_view',
                [
                    'siteaccess' => $this->siteAccess->name,
                    'packageId' => $packageId,
                ]
            );

            return new RedirectResponse($url);
        }
    }

    public function importAction()
    {
        $this->performAccessChecks();

        return $this->render('@ezdesign/content_packages/import/import.html.twig', [
            'types' => $this->importService->getEnabledTypes(),
        ]);
    }

    public function importTypeAction($type)
    {
        $this->performAccessChecks();

        try {
            switch ($type) {
                case ContentPackagesService::SUBTREE_TYPE:
                    $form = $this->getImportSubtreeForm();
                    break;
                case ContentPackagesService::TAGS_TYPE:
                    $form = $this->getImportTagsForm();
                    break;
                case ContentPackagesService::CONTENT_TYPES_TYPE:
                    $form = $this->getImportContentTypesForm();
                    break;
                default:
                    throw new Exception(self::ERROR_MESSAGES['type']);
            }
            $formView = $form->createView();
        } catch (Exception $e) {
            $this->notificationHandler->error(
                str_replace(
                    '$ERROR',
                    $e->getMessage(),
                    self::ERROR_MESSAGES['form_error']
                )
            );
            $url = $this->router->generate(
                'contextual_code_content_packages_dashboard',
                [
                    'siteaccess' => $this->siteAccess->name,
                ]
            );

            return new RedirectResponse($url);
        }

        $params = [
            'form' => $form,
            'form_view' => $formView,
        ];

        return $this->render(
            '@ezdesign/content_packages/import/import_' . $type . '.html.twig',
            $params
        );
    }

    public function importSubmitAction(Request $request)
    {
        $this->performAccessChecks();

        try {
            if ($request->request->has('import_subtree')) {
                $form = $this->getImportSubtreeForm();
                $type = ContentPackagesService::SUBTREE_TYPE;
            } else if ($request->request->has('import_tags')) {
                $form = $this->getImportTagsForm();
                $type = ContentPackagesService::TAGS_TYPE;
            } else {
                $form = $this->getImportContentTypesForm();
                $type = ContentPackagesService::CONTENT_TYPES_TYPE;
            }
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $formData = $form->getData();
            } else {
                throw new Exception($this->getFormErrorMessage($form));
            }

            $adminLogin = $this->getUserLogin();
            $parentId = null;
            if ($type === ContentPackagesService::SUBTREE_TYPE) {
                $parentId = isset($formData[ImportCommand::PARAM_PARENT_ID])
                    ? (int)$formData[ImportCommand::PARAM_PARENT_ID]
                    : null;
                if (
                    $parentId !== null &&
                    !($this->locationService->loadLocation($parentId) instanceof Location)
                ) {
                    throw new Exception(self::ERROR_MESSAGES['parent_location']);
                }
            } else if ($type === ContentPackagesService::TAGS_TYPE) {
                $parentId = isset($formData[ImportCommand::PARAM_PARENT_ID])
                    ? (int)$formData[ImportCommand::PARAM_PARENT_ID]
                    : null;
                if (
                    $parentId !== null &&
                    !($this->tagsService->loadTag($parentId) instanceof Tag)
                ) {
                    throw new Exception(self::ERROR_MESSAGES['parent_tag']);
                }
            }
            /** @var UploadedFile $file */
            $file = $form[ImportSubtreeType::PARAM_FILE]->getData();
            if (!$file) {
                throw new Exception(self::ERROR_MESSAGES['select_file']);
            }

            // clear progress manually
            $packageId = time();
            $entity = $this->importService->clearAndGetStatus($adminLogin, $packageId);
            $entity->setInProgress(true);
            $this->importService->persistStatus($entity);
            $this->importService->setupImportDirectories($adminLogin, $packageId);

            // save file
            $this->importService->moveImportFile(
                $file,
                $adminLogin,
                $parentId,
                $packageId
            );

            $this->importService->extractContentPackage($adminLogin, $packageId);

            $pathName = 'contextual_code_content_packages_import_confirm_no_parent';
            $params = [
                'siteaccess' => $this->siteAccess->name,
                'packageId' => $packageId,
                'type' => $type,
            ];
            if ($parentId !== null) {
                $params['parentId'] = $parentId;
                $pathName = 'contextual_code_content_packages_import_confirm';
            }
            $url = $this->router->generate($pathName, $params);
            return new RedirectResponse($url);
        } catch (Exception $e) {
            $this->notificationHandler->error($e->getMessage());
            $url = $this->router->generate(
                'contextual_code_content_packages_import',
                [
                    'siteaccess' => $this->siteAccess->name,
                ]
            );

            return new RedirectResponse($url);
        }
    }

    public function importConfirmNoParentAction($packageId, $type)
    {
        return $this->forward('EzPlatformContentPackagesBundle:Packages:importConfirm', [
            'packageId' => $packageId,
            'parentLocationId' => null,
            'parentId' => null,
            'type' => $type,
        ]);
    }

    public function importConfirmAction($packageId, $parentId, $type)
    {
        $this->performAccessChecks();

        try {
            $adminLogin = $this->getUserLogin();
            $entity = $this->importService->getStatus($adminLogin, $packageId);

            $metadata = $this->importService->getMetadata(
                $adminLogin,
                $packageId,
                false,
                true
            );

            $params = [
                'metadata' => $metadata,
                'package_id' => $packageId,
                'entity' => $entity,
                'type' => $type,
            ];
            if ($parentId !== null) {
                if ($type === ContentPackagesService::SUBTREE_TYPE) {
                    $parentLocation = $this->locationService->loadLocation($parentId);
                    $params = array_merge(
                        $params,
                        [
                            'parent_name' => $parentLocation->contentInfo->name,
                            'parent_id' => $parentLocation->id,
                        ]
                    );
                } else if ($type === ContentPackagesService::TAGS_TYPE) {
                    $parentTag = $this->tagsService->loadTag($parentId);
                    $params = array_merge(
                        $params,
                        [
                            'parent_name' => $parentTag->keyword,
                            'parent_id' => $parentTag->id,
                        ]
                    );
                }
            }
            return $this->render(
                '@ezdesign/content_packages/import/import_confirm.html.twig',
                $params
            );
        } catch (Exception $e) {
            $this->notificationHandler->error($e->getMessage());
            $url = $this->router->generate(
                'contextual_code_content_packages_import',
                [
                    'siteaccess' => $this->siteAccess->name,
                ]
            );

            return new RedirectResponse($url);
        }
    }

    public function importStartNoParentAction($packageId, $type)
    {
        return $this->forward('EzPlatformContentPackagesBundle:Packages:importStart', [
            'packageId' => $packageId,
            'parentLocationId' => null,
            'type' => $type,
        ]);
    }

    public function importStartAction($packageId, $type = '', $parentId = null)
    {
        $this->performAccessChecks();

        try {
            $adminLogin = $this->getUserLogin();
            $entity = $this->importService->getStatus($adminLogin, $packageId);

            // start import
            $this->importService->importAsync(
                $entity,
                $adminLogin,
                $parentId,
                $packageId,
                $type
            );

            if (!empty($entity->getError())) {
                $error = $entity->getError();
                $this->importService->removeStatus($entity);
                throw new Exception($error);
            }

            $url = $this->router->generate(
                'contextual_code_content_packages_import_view',
                [
                    'siteaccess' => $this->siteAccess->name,
                    'packageId' => $packageId,
                ]
            );

            return new RedirectResponse($url);
        } catch (Exception $e) {
            $this->notificationHandler->error($e->getMessage());
            $url = $this->router->generate(
                'contextual_code_content_packages_import',
                [
                    'siteaccess' => $this->siteAccess->name,
                ]
            );

            return new RedirectResponse($url);
        }
    }

    public function importViewAction($packageId)
    {
        $this->performAccessChecks();

        try {
            $adminLogin = $this->getUserLogin();
            $entity = $this->importService->getStatus($adminLogin, $packageId);
            $statusLog = $this->importService->getStatusLog(
                $adminLogin,
                $packageId,
                false,
                true
            );

            return $this->render(
                '@ezdesign/content_packages/import/import_progress.html.twig',
                [
                    'status_log' => $statusLog,
                    'package_id' => $packageId,
                    'entity' => $entity,
                ]
            );
        } catch (Exception $e) {
            $this->notificationHandler->error($e->getMessage());
            $url = $this->router->generate(
                'contextual_code_content_packages_import',
                [
                    'siteaccess' => $this->siteAccess->name,
                ]
            );

            return new RedirectResponse($url);
        }
    }

    public function aboutAction()
    {
        $this->performAccessChecks();

        return $this->render('@ezdesign/content_packages/about.html.twig');
    }

    protected function getFormErrorMessage(FormInterface $form)
    {
        $formErrors = $form->getErrors(true, true);
        $formErrorStrings = [];
        foreach ($formErrors as $formError) {
            $formErrorStrings[] = $formError->getMessage();
        }
        $message = self::ERROR_MESSAGES['invalid_form'];
        $message .= implode('; ', $formErrorStrings);

        return $message;
    }
}
