<?php

namespace ContextualCode\EzPlatformContentPackagesBundle\FilterIterator;

use RecursiveFilterIterator;

class NoDotRecursiveFilterIterator extends RecursiveFilterIterator
{
    public function accept()
    {
        return strpos($this->current()->getFilename(), '.') !== 0;
    }
}
